#!/bin/sh
#2.1.x
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier

# - configuration - #

#########################################################
# localConcta()                                         #
# concatenne les fichiers css dans ./styles/styles.css  #
# concatenne les fichiers js  dans ./locales/scripts.js #
#########################################################
localConcat() {
        # - concatenation des fichiers css - #
        echo "$couleurINFO # - concatenation des fichiers css dans ./styles/styles.css - #$couleurNORMAL";
#        catCSS ./styles/knacss.css
#        catCSS ./styles/gestLib-0.1.css

#        catCSS ./styles/html4.css
#        catCSS ./styles/intersites.css

#        catCSS ./styles/notes/notesLocales.css
#        catCSS ./styles/menuOnglets-defaut/menuLocales.css
#        catCSS ./styles/tutoriels/tutorielsLocales.css


        # - concatenation des fichiers js - #
        echo "$couleurINFO # - concatenation des fichiers js dans ./locales/scripts.js  - #$couleurNORMAL";
        #catJS /www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js

        # - cp de pages - #
        echo "$couleurINFO # - copie de pages - #$couleurNORMAL";
        cp -R /www/0pages/_site/ ./pagesLocales/_site/

}

##################################################
# localSave()                                    #
# copie un fichier en le suffixant de la version #
##################################################
localSave() {
	echo "$couleurINFO # - localVersion.sh:localSave() - #$couleurNORMAL";

	if [ $isSimulation -eq 0 ];then
		# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
		versionSave ./scripts/gitVersion sh
		versionSave ./localVersion sh

		#exemple:
		#versionSave script js

	fi
}

#######################################
# localStatification()                #
# télécharge et rend statique un site #
#######################################
localStatification(){
	echo "$couleurINFO # - localVersion.sh:localStatification() - #$couleurNORMAL";
	url="http://127.0.0.1/git/legralDocs/legralNet/";

	echo "telechargement avec rendu statique d'un site de $url";

	if [ $isSimulation -eq 0 ];then
		#dirname=${0##*/};		#dirname=`dirname $0`;		#echo "dirname: ${dirname}";
		#gitVersion a mit le rep racine du projet "../scripts" comme rep de travail
		#echo "$couleurINFO repertoire de travail$couleurNORMAL";pwd;

		echo "$couleurINFO suppression et creation de ../statique/$couleurNORMAL";
		rm -R ./statique/;mkdir ./statique/;cd   ./statique/;

		echo "$couleurINFO téléchargement...$couleurWARN";

		#--no-verbose --quiet
		wget --quiet --tries=5 --continue --no-host-directories --html-extension --recursive --level=inf --convert-links --page-requisites --no-parent --restrict-file-names=windows --random-wait --no-check-certificate $url

		echo "$couleurINFO deplacer et nettoyer le chemin$couleurNORMAL";
		#mv ./git/intersites/lib/legral/js/repereTemporel/ ./
		#mv ./git/ ./

		# - decommenter pour activer la suppression apres verification - #
		#rm -R ./git/
	fi
}


#######################################
# syncRemote()                        #
# synchronise le(s) serveurs distants #
#######################################
syncRemote(){
	echo "$couleurINFO # - localVersion.sh:syncRemote() - #$couleurNORMAL";
	# - http://doc.ubuntu-fr.org/lftp - #
	# configurer ~.netrc pour ne pas a avoir a mettre le passsword

	# repertoire local ./repereTemporel sera (ftp.legral.fr/ /rt/repereTemporel
	if [ $isSimulation -eq 0 ];then
		echo "exemple:";
		#lftp -u legral ftp://ftp.legral.fr -e "mirror -e -R  ./statique/vampire   /dijonraxis0/ ; quit"

	fi
}

#######################################
# updateModele()                        #
# met les lib a jours (par linkage) #
#######################################
updateModele(){
	echo "$couleurINFO # - localVersion.sh:updateModele() - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then

		# -- gestionnaire de versions -- #
		lib='gitVersion';
		mkdir -p ./scripts/;
		#version dev
		libV="$lib.sh";echo "$couleurINFO lib:$libV $couleurNORMAL";link /www/git/bash/$lib/scripts/$libV ./scripts/$libV;
		#version fixe
		libV="$lib-v2.0.1.sh";echo "$couleurINFO lib:$libV $couleurNORMAL"; cp /www/git/bash/$lib/versions/scripts/$libV ./scripts/$libV;
		echo "$couleurWARN metter a jours localVersion.sh $couleurNORMAL";

		# -- librairies  -- #
		lib='gestLib';
		mkdir -p ./lib/legral/php/$lib;
		#version dev
		libV="$lib.php";echo "$couleurINFO lib:$libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		libV="$lib.js"; echo "$couleurINFO lib:$libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#version fixe
		libV="$lib-v2.0.0.php";echo "$couleurINFO lib:$libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;
		libV="$lib-v2.0.0.js";echo "$couleurINFO lib:$lib $couleurNORMAL";  cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;
		#styles
		mkdir -p ./styles
		libV="$lib.css";echo "$couleurINFO css:$libV $couleurNORMAL";link /www/git/intersites/lib/legral/php/$lib/styles/$libV ./lib/legral/php/$lib/$libV;

		lib='';

		# -- fichiers communs -- #
		echo "$couleurINFO fichiers communs $couleurNORMAL";
		link /www/git/gitModele/piwik.php ./piwik.php;

		# -- Changement des droits  -- #		echo "$couleurINFO Changement des droits $couleurNORMAL";
		echo "$couleurINFO Changement des droits $couleurNORMAL";
		chmod -R 755 ./scripts/
		chmod -R 755 ./lib/
#		chmod -R 755 ./lib/legral/php
#		chmod -R 755 ./lib/legral/js

	fi
}

#################
# postGit()     #
# lancer en fin #
#################
postGit() {
	echo "$couleurINFO # - localVersion.sh:postGit() - #$couleurNORMAL";
	# if [ $isSimulation -eq 0 ];then
		#
	# fi
}
