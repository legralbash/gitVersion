#!/bin/sh
# - configuration - #
VERSION="2.0.0";
noStatus=0;             # si 1: git status ne sera pas lancer
no0Pages=1;		# si 1: ne monte pas /www/0pages

# - variable interne - #
scriptPath=$0;           #chemin complet du script 
scriptRep=`dirname $0`;  #repertoire du script
localVersionPath="./localVersion.sh";
islocalVersionLoad=0;
isSimulation=0;		# -n ($simulerArg)
isMin=0;
isCommit=0;
commitVersion=0;
commitFail=0;		# egale a 1 si le commit a echoue
isPush=0;
isStatus=0;		# faire un git status en fin d'operation ? (et afficher les tags)
template="";		# creation de rep et fichier (modele de projet)
debug=0;
verbosity=0;
verboseArg='-v';
simulerArg="-n"

# - chargement des codes couleurs - #
. $scriptRep/legralcouleur.sh

echo "$couleurINFO$repScript gitVersion v$VERSION$couleurNORMAL";
echo "gitVersion --help";


#########
# usage #
#########
usage () {
	echo " ./scripts/gitVersion.sh$couleurINFO options$couleurNORMAL";
	echo "$couleurINFO --help$couleurNORMAL";
	echo " --min: concat et minimise les js et css (defini dans ./localVersion.sh)";
	echo " --commit$couleurINFO version$couleurNORMAL:commit les changements et les tag avec version";
	echo " --push: push les commits et les tag ";
	echo " $simulerArg | --simule simule les actions";
	echo " --template$couleurINFO template$couleurNORMAL: intialise les fichiers selon le template";
	echo " --status: faire un git status en fin d'operation (et afficher les tags)";
	echo " sh: le projet est un script shell";
	echo " web: le projet est un site web";
	echo " --status: faire un git status en fin d'operation (et afficher les tags)";
	echo "$couleurINFO exemples:";
	echo "./scripts/gitVersion.sh --template=web ";
	echo "./scripts/gitVersion.sh --simule --min --commit --push --status  $couleurNORMAL";
	exit
	}	

##################
# analyse du psp #
#$0	Contient le nom du script tel qu'il a été invoqué
#$*	L'ensembles des paramètres sous la forme d'un seul argument
#$@	L'ensemble des arguments, un argument par paramètre
#$#	Le nombre de paramètres passés au script
#$?	Le code retour de la dernière commande
#$$	Le PID su shell qui exécute le script
#$!	Le PID du dernier processus lancé en arrière-plan
#
# -v:: --verbose:: argument optionnel
# -V --version
# --in: les cibles (rep/fichiers) provienne de l'entree standart
#
#TEMP=`getopt \
#     --options ab:c:: \
#     --long a-long,b-long:,c-long:: \
#     -n 'example.bash' \
#      -- "$@"
#echo "Remaining arguments:"
#for arg do echo '--> '"\`$arg'" ; done
#
##################
TEMP=`getopt \
     --options v::dVhn \
     --long verbose::,debug,version,help,status,template::,min,commit::,tag::,push \
     -- "$@"`

# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"
#if [ $? -eq 0 ] ; then echo "options requise. Sortie" >&2 ; exit 1 ; fi
#echo "$couleurWARN nb: $# $couleurNORMAL";
if [ $# -eq 1 ] ; then  usage; exit 1 ; fi #pas de parametre


while true ; do
        case "$1" in
                -h|--help)usage; exit 0; shift ;;
                #-v|--verbose) echo "Option b, argument \`$2'" ; shift 2 ;;
                -V|--version) echo "$VERSION"; exit 0; shift ;;
                -v|--verbose)
                        case "$2" in
                                "") verbosity=1; shift 2 ;;
                                *)  #echo "Option c, argument \`$2'" ;
                                        verbosity=$2; shift 2;;
                        esac ;;
		-d|--debug)
                        case "$2" in
                                "") debug=1;  shift 2 ;;
                                *)  debug=$2; shift 2 ;;
                        esac ;;

		-n)isSimulation=1; shift ;;
		--noStatus) noStatus=1; shift ;;
		--template)
			case "$2" in
				"") echo "$couleurWARN nom du modele obligatoire$couleurNORMAL"; exit;  shift 2 ;;
				*) template="$2";  shift 2  ;;
			esac ;;


		--min) isMin=1; shift ;;
			
		--commit)
			case "$2" in
				"") echo "$couleurWARN la version du commit est obligatoire$couleurNORMAL"; exit ; shift 2  ;;
				*) isCommit=1; commitVersion="$2";  shift 2  ;;
			esac ;;

		--push) isPush=1; shift ;;
		--status) isStatus=1; shift ;;



		--) shift ; break ;;
                *) echo "option $1 non reconnu"; shift;  exit 1 ;; # sans exit: si la derniere option est inconnu -> boucle sans fin
        esac
done


	
#############
# functions #
#############
# -- creation du repo git si non existant -- #
createRepoGit() {
rep="./.git";
echo  "$couleurINFO\creation du repo git$couleurNORMAL";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
else
        echo "$couleurWARN repertoire $rep NON existant -> creation d'un repo initial$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		git init;
	fi
        echo "$couleurWARN taper 'git push --set-upstream origin master' pour le positionner sur master $couleurNORMAL";
        fi
}


########################
# creation du template #
########################
createTemplate(){
	case "$template" in
		web) createTemplateWeb ;;
                shell) createTemplateShell ;;
        esac
}


createTemplateCommun(){
	echo "$couleurINFO creation du template: Commun$couleurNORMAL";
	rep="./versions";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi

	# -- creation du repertoire des documents (generales) locales -- #
	rep="./locales";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi

}


createTemplateShell(){
	createTemplateCommun;
	echo "$couleurINFO creation du template: Shell$couleurNORMAL";
	echo "";
}

createTemplateWeb(){
	createTemplateCommun;
	echo "$couleurINFO\creation du template: Web$couleurNORMAL";

	# -- creation du repertoire des styles -- #
	rep="./styles";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi

	# -- creation du repertoire des pages locales -- #
	rep="./pagesLocales";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        echo  "pages specifique a ce projet. Les pages communes sont dans ./pages/ (repertoires montes depuis /www/0pages/" > $rep/readme
	        fi

	# -- creation du repertoire des pages communes et montage -- #
	rep="./pages";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi

	if [ "$no0Pages" -eq 0 ];then
	        echo "montage de /www/0pages dans $rep";
	        fichier="$rep/0pages";
	        if [ -f "$fichier" ];
	        then
	                echo "$couleurINFO presence de $fichier detecte: le repertoire est considere comme deja monte";
	        else
	                echo "si la fonction ne marche pas modifier /etc/sudoers$couleurWARN";
	                sudo mount --read-only --bind /www/0pages $rep
	                echo "$couleurNORMAL";
	                fi
	else
	        echo "$couleurINFO 0Pages non montes suivant configuration$couleurNORMAL";
	        fi

	# - creation/remise a zero des fichiers locaux - #
	echo "$couleurINFO creation/remise a zero des fichiers locales $couleurNORMAL";
	echo "/*! fichier generer automatiquement et compresser avec yuicompressor */" > ./styles/styles.css
	echo  "/*! fichier generer automatiquement et compresser avec yuicompressor */" > ./locales/scripts.js
	echo "";
}


#########################################################
# - generere les fichiers contennue dans ./locales/ ) - #
# - generation de script.js et styles.css -             #
#########################################################
localGenMin() {
	# - chargement config local - #
	if [ $islocalVersionLoad -eq 1 ];
	then

		# - concatenation des fichiers - #
		echo "$couleurINFO localConcat: concatenation des fichier.$couleurNORMAL";
		localConcat;	# cette fonction est dans le fichier $localVersionPath 

		# - minimise scripts.js - # 
		# yuicompressor rajoute automatiquement l'extention ".js|.css" (pas ici! -> rep inside ?)

		echo "compression de ./locales/scripts.js .$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then
			java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./locales/scripts.js -o ./locales/scripts.min.js
		fi
#		echo "suppression de ./locales/scripts.js .$couleurNORMAL";
#		rm ./locales/scripts.js

		echo "compression de ./locales/styles.css .$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then
			java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./styles/styles.css -o ./styles/styles.min.css
		fi
#		echo "suppression de ./locales/styles.css .$couleurNORMAL";
#		rm ./locales/styles.css
		fi
}

##########
# commit #
##########
commit() {
	echo "$couleurINFO on commit...";
	echo "mais avant:";
	echo "1a- on copie la version dans le rep version.";
	if [ $islocalVersionLoad -eq 1 ];
	then
		# - generation de script.js et styles.css - #
		echo "1a - concatenation des css js, et compression des fichiers generer.";
		localGenMin;	# appele source $localVersionPath neccessaire a localSave()

		# - creation des versions et copies dans ./versions - #	
		echo "1b - copie des fichiers versionnees dans ./version";
		localSave;	# cette fonction est dans le fichier $localVersionPath 

	else
		echo "$couleurWARN pas de fichier $localVersionPath$couleurNORMAL";
		fi


	# - mise a jours du fichier contenant la version - #
	if [ $isSimulation -ne 1 ];then
		echo "$couleurINFO mise a jours du fichier lastVersion$couleurNORMAL";
		echo "$PROGVERSION" > ./lastVersion
		fi

	# - On add les fichiers - #
	echo "$couleurINFO 2- on add .$couleurNORMAL";
	git add .

	# - on commit  - #
	if [ $isSimulatioon -ne 1 ];then
		echo "$couleurINFO maintenant on commit (copier le texte issue de release)$couleurNORMAL";
		git commit

		if [ $? -eq 0 ];then
			echo "$couleurINFO on tag le commit avec $couleurWARN$PROGVERSION$couleurNORMAL";
			git tag $PROGVERSION;
		else
			echo "$couleurWARN git commit a renvoyer le code d'erreur $gitErreur";
			commitFail=1;
			fi
	else
		echo "$couleurWARN mode test: pas de commit ni tag $couleurNORMAL";
		fi
}

########
# push #
########
push() {
	if [ $isSimulation -eq 0 ];then
		echo "$couleurINFO on push les commits$couleurNORMAL";
		git push;
		echo "$couleurINFO on push les tags$couleurNORMAL";
		git push --tag
	else
		echo "$couleurWARN mode test: pas de push $couleurNORMAL";
		fi		

} 


######################
# fonction de sortie #
######################
sortieOk() {
	echo "--------------------------";

	if [ $isStatus -eq 1 ];then

		echo "$couleurINFO on affiche le status:$couleurNORMAL"
		git status;

		echo "$couleurINFO on affiche les tags$couleurNORMAL";
		git tag;
	fi
		
	# - postGit : appelle de la function postGit (defini dans localVersion) - #
	postGit;
	exit 0;	#on quitte le script
	}


############################################
# - functions utiliser par localesSave() - #
############################################

# - concatenation de fichier - #
catJS() {
	echo -e  "\r\n/*!                      $1 */" >> ./locales/scripts.js
	echo "$couleurWARN";
        cat "$1"            >> ./locales/scripts.js
	echo "$couleurNORMAL";
        }

catCSS() {
	echo -e  "\r\n/*!                      $1 */" >> ./styles/styles.css
	echo "$couleurWARN";
        cat "$1"            >> ./styles/styles.css
	echo "$couleurNORMAL";
        }


# ----------------------------------------------------------------- #
# - creer une copie d'un js|css en ajoutant la version en suffixe - #
# %1:fn:fichier nom
# %2:fe: fichier extention (sans le point)
# (calc)fv: nom du fichier avec la version en suffixe
versionSave() {
	fn=$1;
	fe=".$2";	# ajout du point 
        fv="$fn-$PROGVERSION";
	echo "creation de la copie $fn$fe vers ./versions/$fv$fe";
        cp $fn$fe ./versions/$fv$fe
	}

# - minimise les fichiers js ou css (selon $2) - #

# minSuf: suffixe de la minification
versionMinimise() {
	fn=$1;
	fe=".$2";
	minSuf=".min";
	fm="$fn$minSuf-$PROGVERSION$fe";
        echo "compression de  $fn$fe vers $fm$fe";
	java -jar ${scriptRep}/scripts/yuicompressor-2.4.8.jar $fn$fe -o ./versions/$fm
        }


########
# main #
########
PROGVERSION=$1;

#if [ $# -eq 0 ];then
#	usage;
#fi

# - on se positionne sur la racine du projet - #
cd $scriptRep/../
echo "$couleurINFO repertoire courant: $couleurNORMAL";
pwd;

# - indique si c'est une simulation - #
if [ $isSimulation -eq 1 ];then
	echo "$couleurWARN simulation aucune action ne sera reelement faite$couleurNORMAL";
fi



# - creation du repo git si non existant - #
createRepoGit;


# - Creation des templates - #
createTemplate;


# - chargement de la configuration utilisateur  - #
if [ -f "$localVersionPath" ];
then
	echo "$couleurINFO Chargement de $localVersionPath $couleurNORMAL";
	. $localVersionPath;
	islocalVersionLoad=1;
else
	echo "$couleurWARN pas de fichier $localVersionPath$couleurNORMAL";
	islocalVersionLoad=0;
	fi


# - minimiser - #
if [ $isMin -eq 1 ];then
        localGenMin;
#        sortieOk;
        fi

# - commit - #
if [ $isCommit -eq 1 ];then
	commit;	# execute localGenMin();
#	if [ "$3" = "push" ];then
#		if [ $commitFail != 0 ];then
#			echo "$couleurWARN Probleme lors du commit: pas de push $couleurNORMAL";
#			fi		
#		push;
#		fi
#	sortieOk;
fi

# - push - #

if [ $isPush -eq 1 ];then
	echo "$couleurINFO push$couleurNORMAL";
	push;
#	sortieOk;
fi

sortieOk;
